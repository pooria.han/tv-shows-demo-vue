class URLs {
  constructor() {
    this.shows = 'https://api.tvmaze.com/shows';
    this.singleShow = 'https://api.tvmaze.com/shows/';
    this.search = 'https://api.tvmaze.com/search/shows';
  }
}

export default new URLs();
