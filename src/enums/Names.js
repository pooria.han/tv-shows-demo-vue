class Names {
  constructor() {
    this.projectTitle = 'TV-Purya';
    this.home = 'home';
    this.detail = 'detail';
    this.trans = {
      tvShowsFromBestToWorst: 'TV Shows from Best to Worst',
      noResult: 'No Result!',
      genres: {
        action: 'Action',
        anime: 'Anime',
        comedy: 'Comedy',
        drama: 'Drama',
        thriller: 'Thriller',
        history: 'History',
        horror: 'Horror',
        family: 'Family',
      },
    };
  }
}

export default new Names();
